﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UILoader : MonoBehaviour
{
    public GameObject CanvasIPhone, CanvasIPad;
    public bool MainMenu = false;

    // Use this for initialization
    void Start()
    {
        if (MainMenu)
        {
            GameObject e, UI = GameObject.Find("Canvas");
#if UNITY_IOS
            if ((UnityEngine.iOS.Device.generation.ToString()).IndexOf("iPad") > -1)
                e = Instantiate(CanvasIPad);
            else
                e = Instantiate(CanvasIPhone);
#elif UNITY_ANDROID || UNITY_EDITOR
            e = Instantiate(CanvasIPhone);
#endif
            e.GetComponent<RectTransform>().rect.Set(0, 0, 0, 0);
            e.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
            UI.GetComponent<CanvasScaler>().screenMatchMode = CanvasScaler.ScreenMatchMode.Expand;
            if (e.name.Contains("MenuOther"))
                UI.GetComponent<CanvasScaler>().referenceResolution = new Vector2(750, 1334);
            else
                UI.GetComponent<CanvasScaler>().referenceResolution = new Vector2(1536, 2048);
            e.transform.SetParent(UI.transform, false);
        }
        else
        {
            GameObject e;
            //e.transform.SetParent(GameObject.Find("Canvas").transform);
#if UNITY_IOS
            if ((UnityEngine.iOS.Device.generation.ToString()).IndexOf("iPad") > -1)
                e = Instantiate(CanvasIPad);
             else
                e = Instantiate(CanvasIPhone);
#elif UNITY_ANDROID || UNITY_EDITOR
            e = Instantiate(CanvasIPhone);
#endif
            AppController.uiAnim = e.GetComponent<Animator>();
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
