﻿using System.Collections;
using System.Collections.Generic;
using NatCorderU.Core;
using NatCorderU.Core.Recorders;
using NatShareU;
using UnityEngine;
using TheNextFlow.UnityPlugins;
using Vuforia;

public class AppController : MonoBehaviour
{
    public static Animator uiAnim;
    //public GameObject CanvasIPad, CanvasOther;
    private Vector2 fp;
    private Vector2 lp;
    WebCamTexture cameraTexture;
    GameObject e;
    public static GameObject CurrentScene;
    public static string CurrentLoadedBundle = "";
    public static bool PrepareForRecord = false;

    public static Object[] ScenePrefabs;
    public Object[] prefabs;

    bool CanUseScreen = false;
    float TimeScinceSceneLoaded;
    // Use this for initialization
    void Start()
    {
        TimeScinceSceneLoaded = Time.realtimeSinceStartup;
        Input.simulateMouseWithTouches = true;
    }

    // Update is called once per frame
    void Update()
    {
        ScreenTouchesProtector();
        //try
        //{            
        //    GameObject.Find("New Game Object").SetActive(false);
        //}
        //catch {; }
        //TrackingHelper();

        if (!CanUseScreen) return;

        uiAnim.SetInteger("Note", NotesController.State);
        if (!SceaneController.SomethingDetected())
            uiAnim.SetBool("Tab", false);

        foreach (Touch touch in Input.touches)
        {
            if (touch.phase == TouchPhase.Began)
            {
                fp = touch.position;
                lp = touch.position;
            }

            if (touch.phase == TouchPhase.Moved)
            {
                lp = touch.position;
            }

            if (touch.phase == TouchPhase.Ended)
            {

                if ((fp.x - lp.x) > 80) // left swipe
                {
                    uiAnim.SetBool("OpenMenu", false);
                }
                else if ((fp.x - lp.x) < -80) // right swipe
                {
                    uiAnim.SetBool("OpenMenu", true);
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.Escape))
            uiAnim.SetBool("OpenMenu", false);

    }

    //void TrackingHelper(){
    //	RaycastHit hit;
    //       Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

    //       if (!Physics.Raycast(ray, out hit) && SceaneController.SomethingDetected()) {
    //		TrackerManager.Instance.GetTracker<ObjectTracker>().Stop();
    //       }
    //}

    void ScreenTouchesProtector()
    {
        if (CanUseScreen) return;

        if (Time.realtimeSinceStartup - TimeScinceSceneLoaded > 3)
            CanUseScreen = true;
        else
        {
            CanUseScreen = false;
            uiAnim.SetBool("OpenMenu", false);
        }
    }
    public void OpenMenu()
    {
        if (CanUseScreen)
            uiAnim.SetBool("OpenMenu", true);
    }

    public void Capture()
    {
        if (!CanUseScreen) return;

        if (uiAnim.GetInteger("State") == 2)
        {
            StartCoroutine(TakeScreenshot());
        }
        else
        {
            uiAnim.SetInteger("State", 2);
            PrepareForRecord = false;
        }
    }

    public IEnumerator TakeScreenshot()
    {
        yield return new WaitForEndOfFrame();
        Time.timeScale = 0;
           
        uiAnim.transform.gameObject.GetComponent<Canvas>().enabled = false;

        Texture2D screenshot = ScreenCapture.CaptureScreenshotAsTexture(1);
        //Texture2D texture = new Texture2D (Screen.width, Screen.height, TextureFormat.RGB24, true); 
        //texture.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0); 
        //texture.Apply(); 

        



#if UNITY_ANDROID
        MobileNativePopups.OpenAlertDialog(
                "Снимок экрана", "Сохранить или поделится фотографией?",
                "Сохранить", "Поделится", "Отмена",
                () => { NatShare.SaveToCameraRoll(screenshot); }, () => { NatShare.ShareImage(screenshot); }, () => {; });
#elif UNITY_IOS
        NatShare.ShareImage(screenshot);
#endif
        uiAnim.transform.gameObject.GetComponent<Canvas>().enabled = true;
        Time.timeScale = 1;
    }

    public void Record()
    {
        if (CanUseScreen && uiAnim.GetInteger("State") != 1)
        {
            uiAnim.SetInteger("State", 1);
            PrepareForRecord = true;
        }
    }

    // public void StartRecording () {

    //     if (!Microphone.IsRecording (Microphone.devices[0]))
    //         Camera.main.gameObject.GetComponent<AudioSource> ().clip = Microphone.Start (Microphone.devices[0], false, 60, 44100);
    //     Debug.Log("Try to record video");
    //     if (!Camera.main.gameObject.GetComponent<AudioSource> ().isPlaying)
    //         Camera.main.gameObject.GetComponent<AudioSource> ().Play ();        
    //         Replay.StartRecording (
    //             Camera.main,
    //             new Configuration (Configuration.Screen.height, Configuration.Screen.width, 30),
    //             OnVideo,
    //             Camera.main.gameObject.GetComponent<AudioSource> (),
    //             true);

    // }



    // public void StopRecording () {
    //     // Stop recording
    //     Debug.Log("Stop video recording");
    //     if (Replay.IsRecording)
    //         Replay.StopRecording ();
    //     if (Microphone.IsRecording (Microphone.devices[0]))
    //         Microphone.End (Microphone.devices[0]);
    // }

    [Header("Recording")]
    public Container container = Container.MP4;

    [Header("Microphone")]
    public bool recordMicrophone;
    public AudioSource microphoneSource;

    private CameraRecorder videoRecorder;
    private AudioRecorder audioRecorder;

    public void StartRecording()
    {
        // First make sure recording microphone is only on MP4
        recordMicrophone &= container == Container.MP4;
        // Create recording configurations // Clamp video width to 720
        var width = 720;
        var height = width * Screen.height / Screen.width;
        var framerate = container == Container.GIF ? 10 : 30;
        var videoFormat = new VideoFormat(width, (int)height, framerate);
        var audioFormat = recordMicrophone ? AudioFormat.Unity : AudioFormat.None;
        // Start recording
        NatCorder.StartRecording(container, videoFormat, audioFormat, OnVideo);
        videoRecorder = CameraRecorder.Create(Camera.main);
        // If recording GIF, skip a few frames to give a real GIF look
        if (container == Container.GIF)
            videoRecorder.recordEveryNthFrame = 5;
        // Start microphone and create audio recorder
        if (recordMicrophone)
        {
            StartMicrophone();
            audioRecorder = AudioRecorder.Create(microphoneSource, true);
        }
    }

    private void StartMicrophone()
    {
#if !UNITY_WEBGL || UNITY_EDITOR // No `Microphone` API on WebGL :(
        // Create a microphone clip
        microphoneSource.clip = Microphone.Start(Microphone.devices[0], true, 60, 48000);
        while (Microphone.GetPosition(Microphone.devices[0]) <= 0) ;
        // Play through audio source
        microphoneSource.timeSamples = Microphone.GetPosition(Microphone.devices[0]);
        microphoneSource.loop = true;
        microphoneSource.Play();
#endif
    }

    public void StopRecording()
    {
        // Stop the microphone if we used it for recording
        if (recordMicrophone)
        {
            Microphone.End(Microphone.devices[0]);
            microphoneSource.Stop();
            audioRecorder.Dispose();
        }
        // Stop the recording
        videoRecorder.Dispose();
        NatCorder.StopRecording();
    }


    void OnVideo(string path)
    {
        Debug.Log("Saved recording to: " + path);


#if UNITY_ANDROID
        MobileNativePopups.OpenAlertDialog(
                "Запись видео", "Сохранить или поделится записанным видео?",
                "Сохранить", "Поделится", "Отмена",
                () => { NatShare.SaveToCameraRoll(path);}, () => { NatShare.ShareMedia(path); }, () => { ; });
#elif UNITY_IOS
        NatShare.ShareMedia(path);
#endif

    }

    public void GotoSite()
    {
        Application.OpenURL("http://www.magicbook.store");
    }

}