﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Book1_Page8_animhelper : MonoBehaviour {
    public Renderer water;
    public float scrollspeed = 0.5f;
    public float offset;
	// Use this for initialization
	void Start () {
        water.GetComponent<Renderer>();
	}
	
	// Update is called once per frame
	void Update () {
       offset -= Time.deltaTime*scrollspeed;
        water.material.mainTextureOffset=new Vector3(0,offset,0);
	}
}
