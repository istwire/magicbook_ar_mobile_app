﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Part : MonoBehaviour {
    public GameObject part,feya,glassFeya,PALOCKA,zrach1,zrach2;
    public Animator anim;
    public float times;
    public bool inviz;
	// Use this for initialization
	void Start () {
        part.active = false;
        if (inviz == false)
        {
            feya.active = false;

            glassFeya.active = false;
            if (PALOCKA != null)
                PALOCKA.active = false;
            anim.enabled = false;
            if (zrach1 != null)
                zrach1.active = false;
            if (zrach2 != null)
                zrach2.active = false;
        }
        else
        {
            feya.active = true;

            glassFeya.active = true;
            if (PALOCKA != null)
                PALOCKA.active = true;
            anim.enabled = true;
            if (zrach1 != null)
                zrach1.active = true;
            if (zrach2 != null)
                zrach2.active = true;

        }



	}

    // Update is called once per frame
    void Update()
    {
        times -= Time.deltaTime;
        if (inviz == false)
        {
            if (times < 6)
            {
                part.active = true;
            }
            if (times < 4)
            {
                anim.enabled = true;
                glassFeya.active = true;
                if (feya != null)
                    feya.active = true;
                if (PALOCKA != null)
                    PALOCKA.active = true;
                if (zrach1 != null)
                    zrach1.active = true;
                if (zrach2 != null)
                    zrach2.active = true;

            }
            if (times < 2)
            {
                part.active = false;
                gameObject.GetComponent<Part>().enabled = false;
            }
        }
        if (inviz == true)
        {
            if (times < 6)
            {
                part.active = true;
            }
            if (times < 4)
            {
                
                glassFeya.active = false;
                if (feya != null)
                    feya.active = false;
                if (PALOCKA != null)
                    PALOCKA.active = false;
                if (zrach1 != null)
                    zrach1.active = false;
                if (zrach2 != null)
                    zrach2.active = false;

            }
            if (times < 2)
            {
                part.active = false;
                gameObject.GetComponent<Part>().enabled = false;
            }
        }
    }
}
