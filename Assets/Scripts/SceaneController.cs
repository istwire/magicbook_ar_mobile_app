﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class SceaneController : MonoBehaviour
{

    public GameObject ARScene;
    public GameObject WarningScene;
    public static GameObject CurrentScene = null;
    GameObject element, warning;
    bool CalculateOnce = true;
    //bool UnloadOnce = false;
    public bool InteractiveMode = false;

    public int BookPage;
    public float Duration;
    public bool CurrentAnimEnd = false;
    //float tmpTimer;
    bool Calculating = false;
    float EndTime = 0;
    bool IsTracking;
    // AssetBundle Assets = null;


    public int SceneIndex;
    float TimeScinceSceneLoaded;

    // Use this for initialization
    void Start()
    {
        TimeScinceSceneLoaded = Time.realtimeSinceStartup;
        if (InteractiveMode)
        {
            Duration = 0;
            Calculating = false;
            CalculateOnce = true;
        }
        else
            Duration = Duration + 7 * Time.unscaledDeltaTime;

        ARScene.tag = "ARScene";
        //tmpTimer = Duration;
        //changeShader();
    }



    IEnumerator CheckTracking(string imageTargetName)
    {
        if (TimeScinceSceneLoaded > 3)
        {
            var imageTarget = GameObject.Find(imageTargetName);
            var trackable = imageTarget.GetComponent<TrackableBehaviour>();
            var status = trackable.CurrentStatus;
            IsTracking = status == TrackableBehaviour.Status.TRACKED;

            if (status != TrackableBehaviour.Status.TRACKED && IsTracking)
            {
                TrackerManager.Instance.GetTracker<ObjectTracker>().Stop();
                TrackerManager.Instance.GetTracker<DeviceTracker>().Stop();
                yield return new WaitForSeconds(0.1f);
                TrackerManager.Instance.GetTracker<ObjectTracker>().Start();
                TrackerManager.Instance.GetTracker<DeviceTracker>().Start();
                Debug.Log("Strange, checking...");
                yield return new WaitForSeconds(0.5f);
                if (status != TrackableBehaviour.Status.TRACKED)
                {
                    IsTracking = false;
                    Debug.Log("Check is failed");
                }
                else
                    Debug.Log("Check is complete");
            }
        }
    }

    // Update is called once per frame
    void Update()
    {


        if (Time.realtimeSinceStartup - TimeScinceSceneLoaded > 3)
        {

            if (GameObject.Find("Camera001"))
                Destroy(GameObject.Find("Camera001"));

            if (InteractiveMode)
            {
                //Debug.Log(Calculating.ToString() + CalculateOnce.ToString() + Duration.ToString());
                if (Duration != 0)
                    Calculating = true;
            }

            if (Calculating)
            {
                if (CalculateOnce)
                {
                    EndTime = (Time.realtimeSinceStartup + Duration) + (Duration * Time.unscaledDeltaTime);
                    CalculateOnce = false;
                    StartCoroutine(CalculateAnimDuration());
                }
            }
            else
            {
                CalculateOnce = true;
                //EndTime = (Time.realtimeSinceStartup + Duration) + (Duration * Time.deltaTime);
                StopCoroutine(CalculateAnimDuration());
            }
            StartCoroutine(CheckTracking(transform.parent.gameObject.name));
            if (IsTracking)
            {
                //Debug.Log("Tracked " + Loader.Url);
                //Loader.CheckBundle();

                //Debug.Log(Loader.Url + " loaded");

                //Loader.LoadedBundle.Unload(false);
                //AppController.CurrentLoadedBundle = Loader.Hash;

                //Debug.Log("Prepared " + Loader.Url);
                AppController.CurrentScene = gameObject;
                if (element == null)
                {

                    element = Instantiate(ARScene, transform, false) as GameObject;
                    element.transform.parent = this.transform;
                    element.transform.localPosition = Vector3.zero;
                    element.transform.localScale = new Vector3(1, 1, 1);

                    if (!InteractiveMode)
                        Calculating = true;
                    else
                        Calculating = false;

                    //Debug.Log("Sceanes activeted");
                }


            }
            else
            {
                if (InteractiveMode)
                    Duration = 0;
                Calculating = false;
                CalculateOnce = true;
                Destroy(element);
                //tmpTimer = Duration;
                CurrentAnimEnd = false;
                transform.parent.rotation = Quaternion.identity;
                EndTime = 0;
                StopCoroutine(CalculateAnimDuration());

            }

            Resources.UnloadUnusedAssets();
            }
        //TrashCleaner();

    }

    // void TrashCleaner()
    // {
    //     if (SomethingDetected() && element == null && Assets != null)
    //         Assets.Unload(false);
    // }

    //static public bool SomethingDetected()
    //{
    //    StateManager sm = TrackerManager.Instance.GetStateManager();

    //    TrackableBehaviour[] activeTrackables = sm.GetActiveTrackableBehaviours().ToArray();

    //    if (activeTrackables.Length > 0)
    //    {
    //        return true;
    //    }
    //    else
    //    {
    //        return false;
    //    }
    //}

    static public bool SomethingDetected()
    {
        return GameObject.FindGameObjectsWithTag("ARScene").Length > 0;
    }

    IEnumerator CalculateAnimDuration()
    {

        if (Duration != 0)
        {
            while (Time.realtimeSinceStartup < EndTime)
            {
                Debug.Log("Current time is " + Time.realtimeSinceStartup.ToString() + ". Time to and must be " + EndTime.ToString());
                CurrentAnimEnd = false;
                yield return null;
            }
            CurrentAnimEnd = true;
            //Debug.Log("Finished!");
            yield return new WaitForEndOfFrame();
        }
    }

}