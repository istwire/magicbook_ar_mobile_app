﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class star : MonoBehaviour {
    public GameObject stars;
    public float times;
    public GameObject music,part;
	// Use this for initialization
	void Start () {
        music.active = false;
        stars.active = false;
	}
	
	// Update is called once per frame
	void Update () {
        Debug.Log(times);
        times -= Time.deltaTime;
        if (times < 3)
        {
            music.GetComponent<AudioSource>().enabled = true;
             
        }

        if (times < 0)
        {
           
            stars.active = true;
        }
        if (times < -8)
        {
            part.active = false;
            stars.active = false;
        }
     

    }
}
