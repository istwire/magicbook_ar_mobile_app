﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NotesController : MonoBehaviour
{
   
    const int NOTHING = 0;
    const int DELAED_NOTHING = 1;
    const int WATCHED_AND_WAITING = 2;
    const int TIME_TO_TAP = 3;

    public static int State = NOTHING;
    
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        SetState();
        //Debug.Log(State);
    }

    void SetState()
    {                 
        if (SceaneController.SomethingDetected() && AppController.CurrentScene.GetComponent<SceaneController>().CurrentAnimEnd)                    
            State = WATCHED_AND_WAITING;

        if (SceaneController.SomethingDetected() && !AppController.CurrentScene.GetComponent<SceaneController>().CurrentAnimEnd)
            State = NOTHING;

        if (!SceaneController.SomethingDetected())
            State = DELAED_NOTHING;
    }

}
