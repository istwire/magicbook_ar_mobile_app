﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Book1_Page3_animhelper : MonoBehaviour {
    float time, halftime;
    public GameObject part,glass,particle;
    public Animator anim;
    static public bool Clicked = false;

    // Use this for initialization
    void Start () {
        //time = (particle.GetComponent<ParticleSystem>().startLifetime/2)+particle.GetComponent<ParticleSystem>().startDelay;
        time = 10;
        //halftime = 0.3f;
        particle.SetActive(false);
        Clicked = false;
    }

    // Update is called once per frame
    void Update()
    {   
        time -= Time.deltaTime;
        
        if (time < 6)
        {
            particle.SetActive(true);                    
        }

        if (time < 4)
        {
            part.SetActive(false);
            glass.SetActive(false);
            
            //StartCoroutine(AnimWait());            
        }
        if (time < 2){
            particle.SetActive(false);
        
            //halftime -= Time.deltaTime;            
            Input.simulateMouseWithTouches=true;
            if (Input.GetMouseButtonDown(0)){
                anim.SetBool("Button",true);
                Clicked = true;
                
            }
            if (!Clicked && SceaneController.SomethingDetected()){
                //if (halftime<0)
                    AppController.uiAnim.SetBool("Tab", true);
            }else
            {
                AppController.uiAnim.SetBool("Tab", false);
                
            }
            if (Clicked)
                this.gameObject.transform.parent.GetComponent<SceaneController>().Duration = 11;
        }
    }

    IEnumerator AnimWait()
    {
        yield return new WaitForSeconds(2.1f);
        StartCoroutine(MagicBegin());
    }

    IEnumerator MagicBegin()
    {
        yield return new WaitForSeconds(1.3f);


        GetComponents<AudioSource>()[1].enabled = true;
    }

}
