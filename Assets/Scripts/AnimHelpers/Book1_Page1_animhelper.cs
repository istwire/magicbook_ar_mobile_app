﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Book1_Page1_animhelper : MonoBehaviour {
    float time, halftime;
    public GameObject part,glass,stick,particle;
    public Animator anim;

    // Use this for initialization
    void Start () {
        //time = (particle.GetComponent<ParticleSystem>().startLifetime/2)+particle.GetComponent<ParticleSystem>().startDelay;
        time = 10;
        anim.enabled = false;
        particle.SetActive(false);
        part.SetActive(false);
        glass.SetActive(false);
        stick.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {   
        time -= Time.deltaTime;
        
        if (time < 6)
        {
            particle.SetActive(true);                    
        }

        if (time < 4)
        {
            part.SetActive(true);
            glass.SetActive(true);
            stick.SetActive(true);            
            StartCoroutine(AnimWait());            
        }
        if (time < 2){
            particle.SetActive(false);
        }
    }

    IEnumerator AnimWait()
    {
        yield return new WaitForSeconds(2.1f);
        anim.enabled = true;
        StartCoroutine(MagicBegin());
    }

    IEnumerator MagicBegin()
    {
        yield return new WaitForSeconds(1.3f);


        GetComponents<AudioSource>()[1].enabled = true;
    }

}
