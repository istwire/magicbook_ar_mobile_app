﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Book1_Page2_animhelper : MonoBehaviour {
    public GameObject stars;
    public float times;
	// Use this for initialization
	void Start () {
        stars.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
        times -= Time.deltaTime;
        if (times < 3)
        {
            GetComponents<AudioSource>()[0].enabled = true;
        }

        if (times < 2)
        {
            stars.SetActive(true);
        }
     

    }
}
