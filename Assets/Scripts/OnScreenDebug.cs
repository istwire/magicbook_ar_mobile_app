﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnScreenDebug : MonoBehaviour {

    UnityEngine.UI.Text debug;

    // Use this for initialization
    void Start () {
        debug = GetComponent<UnityEngine.UI.Text> ();
    }

    // Update is called once per frame
    void Update () {

    }
    void OnEnable () {
        Application.logMessageReceived += HandleLog;
    }
    void OnDisable () {
        Application.logMessageReceived -= HandleLog;
    }
    void HandleLog (string logString, string stackTrace, LogType type) {
        if (!debug.text.Contains (logString)) {
            debug.text += " \n ";
            if (type == LogType.Log) {
                debug.text += "[" + Mathf.FloorToInt (Time.realtimeSinceStartup).ToString () + "] <color=white>" + type.ToString () + "</color>: " + logString;
            } else {
                if (type == LogType.Warning){
                    debug.text += "[" + Mathf.FloorToInt (Time.realtimeSinceStartup).ToString () + "] <color=yellow>" + type.ToString () + "</color>: " + logString;}
                else
                    debug.text += "[" + Mathf.FloorToInt (Time.realtimeSinceStartup).ToString () + "] <color=red>" + type.ToString () + "</color>: " + logString;
            }

        }
    }
}