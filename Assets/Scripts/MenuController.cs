﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{


    //public bool LoadingData, LoadingDataError, SelectScene, BackgroundSelect;
    public GameObject Scene, Background, LoadingData, LoadingDataDots;
    bool Ready = false;
    bool SceneLoading = false;
    // float AnimTextDelay = 0.5f;
    // Use this for initialization
    IEnumerator Start()
    {
        Background.SetActive(false);
        Scene.SetActive(false);
        LoadingData.SetActive(false);

        PlayerPrefs.SetInt("boy-girl", -1);

        //Caching.ClearCache();
        QualitySettings.vSyncCount = 0;
        //Application.targetFrameRate = 30;

        if (!Microphone.Start(Microphone.devices[0], true, 1, 48000))
            Application.Quit();
        else
        {
            yield return new WaitForEndOfFrame();
            Microphone.End(Microphone.devices[0]);
        }
        yield return new WaitForSecondsRealtime(3);
        Ready = true;
    }

    IEnumerator AnimateDots()
    {
       yield return new WaitForSecondsRealtime(3f);
       Text a = LoadingDataDots.GetComponent<Text>();
       if (a.text!="...")
           a.text+=".";
       else
           a.text=".";
    }

    // Update is called once per frame
    void Update()
    {    

        if (Ready && !SceneLoading)
        {

           
            if (PlayerPrefs.GetInt("boy-girl") == 1)
                LoadBoy();
            if (PlayerPrefs.GetInt("boy-girl") == 2)
                LoadGirl();

            if (Input.GetMouseButtonDown(0))
            {
                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                if (Physics.Raycast(ray, out hit))
                {
                    Debug.Log(hit.transform.gameObject.name);
                    GameObject Target = hit.transform.gameObject;
                    if (Target.name == "boy")
                    {
                        PlayerPrefs.SetInt("boy-girl", 1);
                        LoadingData.SetActive(true);    
                    }
                    if (Target.name == "girl")
                    {
                        PlayerPrefs.SetInt("boy-girl", 2);
                        LoadingData.SetActive(true);
                    }
                    Debug.Log(PlayerPrefs.GetInt("boy-girl").ToString()+" - status");
                }
            }


            if (PlayerPrefs.GetInt("boy-girl") == -1 || !PlayerPrefs.HasKey("boy-girl"))
            {
                PlayerPrefs.SetString("ver", Application.version);

                Background.SetActive(true);
                Scene.SetActive(true);
            }
        }
        if (SceneLoading)
            StartCoroutine("AnimateDots");
    }

    public void LoadBoy()
    {
        if (ObbExtractor.Extracted)
        {
            PlayerPrefs.SetInt("boy-girl", 1);
            StartCoroutine(LoadMainScene("MainScene_boy"));
        }
    }

    public void LoadGirl()
    {
        if (ObbExtractor.Extracted)
        {
            PlayerPrefs.SetInt("boy-girl", 2);
            StartCoroutine(LoadMainScene("MainScene_girl"));
        }
    }
    
    IEnumerator LoadMainScene(string scene)
    {
        Background.SetActive(false);
        Scene.SetActive(false);
        yield return new WaitForEndOfFrame();

        AsyncOperation async = SceneManager.LoadSceneAsync(scene);
        SceneManager.LoadScene(scene);
        SceneLoading =true;
        // while (!async.isDone)
        //     yield return 
    }

    public void Exit()
    {
        Application.Quit();
    }


}
